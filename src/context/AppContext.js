import React from "react";
import { authReducer } from '../reducers/AuthReducer';

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [blogs, setBlogs] = React.useState([]);
  const [user, dispatch] = React.useReducer(authReducer, {}, init);

  React.useEffect(function saveUserInStorage() {
    localStorage.setItem('user', JSON.stringify(user))
  }, [user]);

  React.useEffect(function saveData() {
    const paragraphs = [
      {
        id: 1,
        title: "Lorem Ipsum 1",
        paragraph: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisi. Vestibulum id tincidunt ligula. Suspendisse potenti. Ut ut eleifend lectus. Integer in nunc at nulla ultrices vulputate.",
        image: "https://example.com/image1.jpg"
      },
      {
        id: 2,
        title: "Lorem Ipsum 2",
        paragraph: "Proin id augue et tortor blandit hendrerit. Curabitur suscipit nisi id nisi luctus, et fringilla purus tristique. Sed auctor ex ac eros malesuada, eu varius odio fermentum.",
        image: "https://example.com/image2.jpg"
      },
      {
        id: 3,
        title: "Lorem Ipsum 3",
        paragraph: "Maecenas ac lacus vitae mi volutpat gravida eget in felis. Phasellus ut metus nec mauris congue accumsan. Fusce fringilla euismod justo, eu accumsan lacus bibendum vel.",
        image: "https://example.com/image3.jpg"
      },
      {
        id: 4,
        title: "Lorem Ipsum 4",
        paragraph: "Vivamus sed sapien ut ex convallis rhoncus. Fusce eu dolor non tortor feugiat feugiat. Integer euismod tincidunt justo, vel commodo dolor euismod a.",
        image: "https://example.com/image4.jpg"
      },
      {
        id: 5,
        title: "Lorem Ipsum 5",
        paragraph: "Cras ultricies velit nec turpis dignissim, vitae tincidunt nulla venenatis. Nunc consectetur quam a nisi luctus, id tincidunt metus posuere.",
        image: "https://example.com/image5.jpg"
      },
      {
        id: 6,
        title: "Lorem Ipsum 6",
        paragraph: "Integer congue, odio eu tincidunt euismod, libero risus scelerisque nunc, ut rhoncus velit lectus ac justo. Mauris bibendum, ante a commodo malesuada, arcu odio lacinia est.",
        image: "https://example.com/image6.jpg"
      },
    ];
    setBlogs(paragraphs);
  }, []);

  // then replace by hook use fetch
  // const fetchResources = React.useCallback(async (url) => {
  //   let response = await fetch(url, { method: "GET" });
  //   let parsedInfo = await response.json();

  //   return parsedInfo;
  // }, []);

  // React.useEffect(
  //   function getResources() {
  //     const url = "https://my-json-server.typicode.com/improvein/dev-challenge/db";

  //     fetchResources(url)
  //     .then((data) => {
  //       setFullData(data);
  //       setBands(data.bands);
  //       setAlbums(data.albums);
  //       setGenres(data.genre);
  //     });
  //   },
  //   [fetchResources]
  // );

  function init() {
    return JSON.parse(localStorage.getItem("user")) || { isLogged: false };
  }

  function getBlogs() {
    return blogs;
  }

  function getBlogById(blogId) {
    return blogs.filter(blog => blog.id === blogId);
  }

  const updateBlog = (id, modifiedTitle, modifiedParagraph) => {
    setBlogs((prevContent) =>
      prevContent.map((content) =>
        content.id === id
          ? {
              ...content,
              title: modifiedTitle || content.title,
              paragraph: modifiedParagraph || content.paragraph,
            }
          : content
      )
    );
  };

  const state = {
    blogs,
    user
  };

  const actions = {
    getBlogs,
    getBlogById,
    updateBlog,
    dispatch
  };

  return (
    <AppContext.Provider value={{ state, actions }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContext as default, AppProvider as Provider };