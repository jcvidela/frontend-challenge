import * as React from "react";
import { useParams, Redirect } from "react-router-dom";
import { useGetBlogsById, useUpdateBlogs } from "../hooks";

export default function () {
  const { blogId } = useParams();
  const getBlogById = useGetBlogsById();
  const updateBlog = useUpdateBlogs();
  const [texto, setTexto] = React.useState('');

  let parsedBlogId = Number(blogId);
  const blog = getBlogById(parsedBlogId)[0];

  if (!blog) return <Redirect to="/home" />;

  React.useEffect(() => {
    setTexto(blog?.paragraph)
  }, [blog])

  function handleUpdateMsg() {
    updateBlog(blog.id, null, texto)
  };

  function handleChange(event){
    setTexto(event.target.value);
  };

  return (
    <>
      <h1 className="text-center">Detail screen</h1>
      <div className="container responsiveGrid">
        
      <div>
        <textarea value={texto} onChange={handleChange} rows={10} cols={50} />
        <br />
        <button onClick={handleUpdateMsg}>Guardar</button>
    </div>
      </div>
    </>
  );
}