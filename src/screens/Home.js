import React from "react";
import { useBlogs } from "../hooks";
import { List, Spinner } from "../components";
import { useHistory } from "react-router-dom";

// eslint-disable-next-line
export default function () {
  let history = useHistory();
  let blogs = useBlogs();

  function onPreviewClick(id) {
    history.push('/blog/' + id);
  }

  return (
    <>
      <h1 className="text-center">Blog personal de Juanca</h1>

      <div>
        {
          blogs.map(blog => {
            return(
              <div  className="list-item" onClick={() => onPreviewClick(blog.id)}>
                <h1>{blog.title}</h1>
                <h4>{blog.paragraph}</h4>
                <p>{blog.image}</p>
              </div>
            )
          })
        }
      </div>
    </>
  );
}
