import * as React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Navbar } from "../layout";
import { Home, Detail } from "../screens";

export default function () {
  return (
    <>
      <Navbar />
      <div>
        <Switch>
          <Route exact path="/home" component={Home} />
          <Route exact path="/blog/:blogId" component={Detail} />

          <Redirect to="/home" />
        </Switch>
      </div>
    </>
  );
}
