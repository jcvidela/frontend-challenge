import React from "react";
import AppContext from "../context/AppContext";

export function useBlogs() {
  const {
    state: { blogs },
  } = React.useContext(AppContext);

  return blogs;
}

export function useGetBlogsById() {
  const {
    actions: { getBlogById },
  } = React.useContext(AppContext);

  return getBlogById;
}

export function useUpdateBlogs() {
  const {
    actions: { updateBlog },
  } = React.useContext(AppContext);

  return updateBlog;
}

export function useAuthDispatch() {
  const {
    actions: { dispatch },
  } = React.useContext(AppContext);

  return dispatch;
}

export function useAuthState() {
  const {
    state: { user },
  } = React.useContext(AppContext);

  return user;
}